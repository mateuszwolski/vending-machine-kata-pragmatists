package tdd.vendingMachine;

import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.CoinsLevelListener;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.shelves.IShelve;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 18.01.2017.
 */
public class AdministratorPanel implements CoinsLevelListener {

    private CashRegister register = CashRegister.getInstance();

    public void addMoneyToCashRegister(DenominationsOfCoins doc, int count) {
        register.addMoney(doc, count);
    }

    public void addProductToShelve(VendingMachine machine, String numberShelve, int productsCount) {
        machine.getShelveForUpdate(numberShelve).addMoreProducts(productsCount);
    }

    public void addNewShelveWithProducts(VendingMachine machine, IShelve shelve) {
        machine.insertShelve(shelve);
    }

    public BigDecimal takeAllMoney() {
        return register.takeAllMoney();
    }

    @Override
    public void lowCoinsLevel(DenominationsOfCoins doc) {
        System.out.print("******* Low level of coins " + doc.getValue() + " *****\n");
    }
}
