package tdd.vendingMachine;

import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.shelves.IShelve;
import tdd.vendingMachine.states.CoinsInsertedState;
import tdd.vendingMachine.states.IState;
import tdd.vendingMachine.states.NoCoinsInsertedState;
import tdd.vendingMachine.states.ProductChosenState;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static tdd.vendingMachine.utils.Utils.optGetInt;

public class VendingMachine {
    private IState coinsInsertedState = new CoinsInsertedState(this);
    private IState noCoinsInsertedState = new NoCoinsInsertedState(this);
    private IState productChooseState = new ProductChosenState(this);
    private IState machineState = noCoinsInsertedState;

    private Map<Integer, IShelve> shelves = new HashMap<>();
    private Map<DenominationsOfCoins, Integer> currentCoinsState = new HashMap<>();
    private Integer chosenShelveNumber = -1;
    private IShelve chosenShelve = null;

    public void insertCoin(DenominationsOfCoins coin) {
        machineState.insertCoin(coin);
    }

    public void selectProduct(String shelve) {
        machineState.chosenProductButton(shelve);
    }

    public void cancelOperation() {
        machineState.cancel();
    }

    public void waiting() {
        machineState.waiting();
    }

    public void insertShelve(IShelve shelve) {
        Integer o = nextShelveNumber();
        shelves.put(o + 1, shelve);
    }

    public void removeProductFromShelve(Integer shelveNumber) {
        shelves.get(shelveNumber).sellProduct();
    }

    public IShelve getShelveForUpdate(String number) {
        return null;
    }

    public IState getCoinsInsertedState() {
        return coinsInsertedState;
    }

    public IState getNoCoinsInsertedState() {
        return noCoinsInsertedState;
    }

    public IState getProductChooseState() {
        return productChooseState;
    }

    public Map<Integer, IShelve> getShelves() {
        return shelves;
    }

    public void setMachineState(IState machineState) {
        this.machineState = machineState;
    }

    public IState getMachineState() {
        return machineState;
    }

    public Map<DenominationsOfCoins, Integer> getCurrentCoinsState() {
        return currentCoinsState;
    }


    public void addCointPocketState(DenominationsOfCoins doc) {
        this.currentCoinsState.compute((doc), (x, old) -> old == null ? 1 : old + 1);
    }

    public void flushCurrentCoinsState() {
        this.currentCoinsState = new HashMap<>();
    }


    public Integer getChosenShelveNumber() {
        return chosenShelveNumber;
    }

    public void setChosenShelveNumber(Integer chosenShelveNumber) {
        this.chosenShelveNumber = chosenShelveNumber;
    }

    public IShelve getChosenShelve() {
        return chosenShelve;
    }

    public void setChosenShelve(IShelve chosenShelve) {
        this.chosenShelve = chosenShelve;
    }

    private Integer nextShelveNumber() {
        return optGetInt(
            shelves
                .keySet()
                .stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList()),
            $ -> !$.isEmpty(), $ -> $.get(0)
        );
    }
}
