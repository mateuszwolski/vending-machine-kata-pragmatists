package tdd.vendingMachine.states;

import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.register.DenominationsOfCoins;

import static tdd.vendingMachine.utils.Screen.*;
import static tdd.vendingMachine.utils.Utils.optDo;
import static tdd.vendingMachine.utils.Utils.sumRegister;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class CoinsInsertedState implements IState {

    private VendingMachine machine;

    public CoinsInsertedState(VendingMachine machine) {
        this.machine = machine;
    }

    @Override
    public void chosenProductButton(String shelve) {
        machine.setMachineState(machine.getProductChooseState());
        machine.selectProduct(shelve);
    }

    @Override
    public void insertCoin(DenominationsOfCoins coin) {
        machine.addCointPocketState(coin);
        yourCredit(sumRegister(machine.getCurrentCoinsState()));
    }

    @Override
    public void cancel() {
        machine.setChosenShelve(null);
        optDo(machine.getCurrentCoinsState(), $ -> !$.keySet().isEmpty(), $ -> {
            returnInsertedCoins($);
            machine.flushCurrentCoinsState();
            machine.setMachineState(machine.getNoCoinsInsertedState());
        });
        welcomeText(machine.getShelves());
    }

    @Override
    public void waiting() {
        chooseProduct();
    }
}
