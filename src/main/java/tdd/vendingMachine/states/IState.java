package tdd.vendingMachine.states;

import tdd.vendingMachine.register.DenominationsOfCoins;

/**
 * Created by Mateusz on 16.01.2017.
 */
public interface IState {
    void chosenProductButton(String shelve);

    void insertCoin(DenominationsOfCoins coin);

    void cancel();

    void waiting();
}
