package tdd.vendingMachine.states;

import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.DenominationsOfCoins;

import java.math.BigDecimal;
import java.util.Map;

import static tdd.vendingMachine.utils.Screen.*;
import static tdd.vendingMachine.utils.Utils.*;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class ProductChosenState implements IState {

    private VendingMachine machine;
    private CashRegister register = CashRegister.getInstance();

    public ProductChosenState(VendingMachine machine) {
        this.machine = machine;
    }

    @Override
    public void chosenProductButton(String shelve) {
        boolean result = validateShelve(shelve);
        optDo(result, $ -> $, $ -> {
            Integer shelveNumber = Integer.parseInt(shelve);
            machine.setChosenShelve(machine.getShelves().get(shelveNumber));
            machine.setChosenShelveNumber(shelveNumber);
            creditCollecting();
        });
        optDo(result, $ -> !$, $ -> machine.setMachineState(machine.getCoinsInsertedState()));
    }

    @Override
    public void insertCoin(DenominationsOfCoins coin) {
        machine.addCointPocketState(coin);
        creditCollecting();
    }

    @Override
    public void cancel() {
        optDo(machine.getCurrentCoinsState(), $ -> !$.keySet().isEmpty(), $ -> {
            returnInsertedCoins($);
            cleanChoices();
            machine.setMachineState(machine.getNoCoinsInsertedState());
        });
        welcomeText(machine.getShelves());
    }

    @Override
    public void waiting() {
        notEnoughtMoney(sumRegister(machine.getCurrentCoinsState()), machine.getChosenShelve().getProduct().getPrice());
    }

    private void creditCollecting() {
        BigDecimal credit = sumRegister(machine.getCurrentCoinsState());
        Integer shelveNumber = machine.getChosenShelveNumber();
        optDo(credit, $1 -> $1.compareTo(machine.getChosenShelve().getProduct().getPrice()) < 0, $1 -> notEnoughtMoney($1, machine.getChosenShelve().getProduct().getPrice()));
        optDo(credit, $1 -> $1.compareTo(machine.getChosenShelve().getProduct().getPrice()) >= 0, $1 -> finishOperation(shelveNumber));
    }

    private boolean validateShelve(String shelve) {
        Integer number = -1;
        boolean result = false;
        try {
            number = Integer.parseInt(shelve);
        } catch (NumberFormatException nfe) {
            wrongShelve(shelve);
            return false;
        }
        result = optGetBool(machine.getShelves().get(number), $ -> $.availableProductsNumber() > 0);
        optDo(result, $ -> !$, $ -> wrongShelve(shelve));
        return result;
    }

    private void finishOperation(Integer shelveNumber) {
        boolean changeMoneyPossible = register.isItPossibleToReturnChange(sumRegister(machine.getCurrentCoinsState()), machine.getChosenShelve().getProduct().getPrice());
        optDo(changeMoneyPossible, $ -> !$, $ -> {
            System.out.println("***Can't change money, here are your money.***");
            cancel();
        });

        optDo(changeMoneyPossible, $ -> $, $ -> {
            machine.removeProductFromShelve(shelveNumber);
            Map<DenominationsOfCoins, Integer> change = register
                .changeMoney(sumRegister(machine.getCurrentCoinsState()), machine.getChosenShelve().getProduct().getPrice());
            change.keySet().stream().forEach($1 -> register.removeMoney($1, change.get($1)));
            returnChange(change, sumRegister(machine.getCurrentCoinsState()), machine.getChosenShelve().getProduct().getPrice());
            System.out.println(productDescribe(machine.getChosenShelve().getProduct()));
            machine.getCurrentCoinsState().keySet().stream().forEach($1 -> register.addMoney($1, machine.getCurrentCoinsState().get($1)));
            machine.setMachineState(machine.getNoCoinsInsertedState());
            cleanChoices();
        });
    }

    private void cleanChoices() {
        machine.flushCurrentCoinsState();
        machine.setChosenShelve(null);
        machine.setChosenShelveNumber(-1);
        welcomeText(machine.getShelves());
    }
}
