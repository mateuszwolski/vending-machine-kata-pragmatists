package tdd.vendingMachine.states;

import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.register.DenominationsOfCoins;

import static tdd.vendingMachine.utils.Screen.*;
import static tdd.vendingMachine.utils.Utils.optDo;
import static tdd.vendingMachine.utils.Utils.optGetBool;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class NoCoinsInsertedState implements IState {

    private VendingMachine machine;

    public NoCoinsInsertedState(VendingMachine machine) {
        this.machine = machine;
    }

    @Override
    public void chosenProductButton(String shelve) {
        optDo(validateShelve(shelve), $ -> $, $ -> {
            Integer shelveNumber = Integer.parseInt(shelve);
            System.out.println(productDescribe(machine.getShelves().get(shelveNumber).getProduct()));
        });
    }

    @Override
    public void insertCoin(DenominationsOfCoins coin) {
        machine.setMachineState(machine.getCoinsInsertedState());
        machine.insertCoin(coin);
    }

    @Override
    public void cancel() {
        welcomeText(machine.getShelves());
    }

    @Override
    public void waiting() {
        welcomeText(machine.getShelves());
    }

    private boolean validateShelve(String shelve) {
        Integer number = -1;
        boolean result = false;
        try {
            number = Integer.parseInt(shelve);
        } catch (NumberFormatException nfe) {
            wrongShelve(shelve);
            return false;
        }
        result = optGetBool(machine.getShelves().get(number), $ -> $.availableProductsNumber() > 0);
        optDo(result, $ -> !$, $ -> wrongShelve(shelve));
        return result;
    }
}
