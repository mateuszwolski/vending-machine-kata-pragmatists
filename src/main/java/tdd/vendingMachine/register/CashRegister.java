package tdd.vendingMachine.register;

import java.math.BigDecimal;
import java.util.*;

import static tdd.vendingMachine.utils.Utils.addToList;
import static tdd.vendingMachine.utils.Utils.sumRegister;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class CashRegister{

    private Map<DenominationsOfCoins, Integer> register = new HashMap<>();

    private List<CoinsLevelListener> observers = new ArrayList<>();

    private CashRegister() {
    }

    private static class CashRegisterHelper {
        private static final CashRegister INSTANCE = new CashRegister();
    }

    public static CashRegister getInstance() {
        return CashRegisterHelper.INSTANCE;
    }

    public void addMoney(DenominationsOfCoins denominationsOfCoins, final Integer count) {
        register.compute(denominationsOfCoins, (x, old) -> old == null ? count : old + count);
    }

    public void removeMoney(DenominationsOfCoins denominationsOfCoins, final Integer count) {
        register.compute(denominationsOfCoins, (x, old) -> old == null ? 0 : old - count);
        if (register.get(denominationsOfCoins) <= 2) {
            observers.stream().forEach($ -> $.lowCoinsLevel(denominationsOfCoins));
        }
    }

    public Map<DenominationsOfCoins, Integer> getRegister() {
        return register;
    }

    public boolean isItPossibleToReturnChange(BigDecimal insertedValue, BigDecimal productCost) {
        return changeMoneyChecker(insertedValue, productCost) != null;
    }

    public Map<DenominationsOfCoins, Integer> changeMoney(BigDecimal insertedValue, BigDecimal productCost) {
        return changeMoneyChecker(insertedValue, productCost);
    }

    public BigDecimal takeAllMoney() {
        BigDecimal sum = sumRegister(register);

        register = new HashMap<>();
        return sum;
    }

    public void addObserver(CoinsLevelListener coinsLevelListener){
        observers.add(coinsLevelListener);
    }

    public void remveObserver(CoinsLevelListener coinsLevelListener){
        observers.remove(coinsLevelListener);
    }

    private Map<DenominationsOfCoins, Integer> changeMoneyChecker(BigDecimal insertedValue, BigDecimal productCost) {
        BigDecimal moneyToReturn = insertedValue.subtract(productCost);
        Map<DenominationsOfCoins, Integer> calc = new HashMap<>();
        List<BigDecimal> registryInArray = new ArrayList<>();
        register.keySet().forEach($ -> addToList($, register.get($), registryInArray));
        registryInArray.sort((o1, o2) -> o2.compareTo(o1));

        for (BigDecimal bd : registryInArray) {
            if (moneyToReturn.compareTo(BigDecimal.ZERO) > 0 && bd.compareTo(moneyToReturn) <= 0) {
                moneyToReturn = moneyToReturn.subtract(bd);
                calc.compute(DenominationsOfCoins.getValue(bd.toPlainString()), (x, old) -> old == null ? 1 : old + 1);
            }
        }
        return moneyToReturn.compareTo(BigDecimal.ZERO) == 0 ? calc : null;
    }
}
