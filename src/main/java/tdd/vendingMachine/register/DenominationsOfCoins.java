package tdd.vendingMachine.register;

/**
 * Created by Mateusz on 16.01.2017.
 */
public enum DenominationsOfCoins {
    FIVE("5.0"),
    TWO("2.0"),
    ONE("1.0"),
    FIFTY_PENS("0.5"),
    TWENTY_PENS("0.2"),
    TEN_PENS("0.1");

    private String value;

    private DenominationsOfCoins(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static boolean isValueExists(String valueToValidate) {
        for (DenominationsOfCoins doc : DenominationsOfCoins.values()) {
            if (doc.value.equals(valueToValidate)) {
                return true;
            }
        }
        return false;
    }

    public static DenominationsOfCoins getValue(String value) {
        for (DenominationsOfCoins doc : DenominationsOfCoins.values()) {
            if (doc.value.equals(value)) {
                return doc;
            }
        }
        return null;
    }

}
