package tdd.vendingMachine.register;

/**
 * Created by Mateusz on 19.01.2017.
 */
public interface CoinsLevelListener {

    void lowCoinsLevel(DenominationsOfCoins doc);
}
