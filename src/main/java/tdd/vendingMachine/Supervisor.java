package tdd.vendingMachine;

import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.utils.InitMachine;

import java.util.Scanner;

import static tdd.vendingMachine.utils.Screen.instruction;
import static tdd.vendingMachine.utils.Utils.optDo;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class Supervisor {
    public static void main(String[] args) {
        instruction();
        VendingMachine machine = new VendingMachine();
        AdministratorPanel admin = new AdministratorPanel();
        CashRegister.getInstance().addObserver(admin);

        InitMachine.initMachine(machine);

        Scanner scanner = new Scanner(System.in);
        String option;
        machine.waiting();
        do {
            option = scanner.nextLine();
            String[] values = option.split(" ");
            switch (values[0].toLowerCase()) {
                case "p":
                    machine.selectProduct(values[1]);
                    break;
                case "m":
                    optDo(DenominationsOfCoins.getValue(values[1]), $ -> $!= null, $ -> machine.insertCoin(DenominationsOfCoins.getValue(values[1])));
                    break;
                case "c":
                    machine.cancelOperation();
                    break;
            }
            System.out.println(".....");
        } while (!option.equalsIgnoreCase("exit"));
    }
}
