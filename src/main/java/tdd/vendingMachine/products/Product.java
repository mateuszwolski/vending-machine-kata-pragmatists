package tdd.vendingMachine.products;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class Product extends AbstractProduct {

    public Product(BigDecimal price, String name) {
        super.price = price;
        super.name = name;
    }
}
