package tdd.vendingMachine.products;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 17.01.2017.
 */
public class Coke extends AbstractProduct {

    public Coke(BigDecimal price, String name) {
        super.price = price;
        super.name = name;
    }

}
