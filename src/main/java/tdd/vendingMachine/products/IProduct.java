package tdd.vendingMachine.products;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 16.01.2017.
 */
public interface IProduct {
    void changePrice(BigDecimal price);

    BigDecimal getPrice();

    String getName();
}
