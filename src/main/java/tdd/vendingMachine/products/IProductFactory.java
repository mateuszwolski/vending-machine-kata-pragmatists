package tdd.vendingMachine.products;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 17.01.2017.
 */
@FunctionalInterface
public interface IProductFactory {
    IProduct createProduct(BigDecimal price, String name);
}
