package tdd.vendingMachine.products;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 17.01.2017.
 */
public abstract class AbstractProduct implements IProduct {

    BigDecimal price;
    String name;

    @Override
    public void changePrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public String getName() {
        return name;
    }

}
