package tdd.vendingMachine.utils;

import tdd.vendingMachine.products.IProduct;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.shelves.IShelve;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by Mateusz on 17.01.2017.
 */
public class Screen {

    public static void welcomeText(Map<Integer, IShelve> shelves) {
        StringBuilder sb = new StringBuilder();
        sb.append("***Welcome***").append("\n");
        sb.append("Choose product from list").append("\n");
        shelves.keySet().stream().forEach($ -> sb.append(productDescribe($, shelves.get($))));
        sb.append("Then insert money").append("\n");
        sb.append("At the end press the button or cancel order and take money back").append("\n");
        System.out.println(sb);
    }

    public static void wrongShelve(String shelve) {
        StringBuilder sb = new StringBuilder();
        sb.append("Sorry you try choose not existing shelve: ").append(shelve);
        sb.append("Please choose different one.");
        System.out.println(sb);
    }

    public static void notEnoughtMoney(BigDecimal currentState, BigDecimal price) {
        StringBuilder sb = new StringBuilder();
        sb.append("Product price: ").append(price.toPlainString()).append("\n");
        sb.append("Credit: ").append(currentState.toPlainString()).append("\n");
        System.out.println(sb);
    }

    public static void chooseProduct() {
        System.out.println("Choose product");
    }


    public static void returnInsertedCoins(Map<DenominationsOfCoins, Integer> currentCredits) {
        StringBuilder sb = new StringBuilder();
        sb.append("Returned denomination : coins count").append("\n");
        currentCredits.keySet().stream().map($ -> sb.append("> denomination: " + $ + " , count: " + currentCredits.get($)).append("\n"));
        System.out.println(sb);
    }

    public static void returnChange(Map<DenominationsOfCoins, Integer> change, BigDecimal insertedValue, BigDecimal productValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("Your credit: ").append(insertedValue.toPlainString()).append("\n");
        sb.append("Product costs: ").append(productValue.toPlainString()).append("\n");
        sb.append("Change money: ").append("\n");
        change.keySet().stream().forEach($ -> sb.append("> denomination: " + $.getValue() + " , count: " + change.get($)).append("\n"));
        System.out.print(sb);
    }

    public static String productDescribe(IProduct product) {
        return "Product name: " + product.getName() + " price: " + product.getPrice().toPlainString();
    }

    public static void yourCredit(BigDecimal bigDecimal) {
        System.out.println("Credit: " + bigDecimal.toPlainString());
    }

    public static void instruction() {
        StringBuilder sb = new StringBuilder();
        sb.append("***Instruction***").append("\n");
        sb.append("p 11 means that you want to choose product from shelve 11").append("\n");
        sb.append("m 1.0 means that you insert in to machine 1.0").append("\n");
        sb.append("c means that you want to cancel operation").append("\n");
        sb.append("exit exit").append("\n\n");
        System.out.print(sb);
    }

    private static String productDescribe(Integer number, IShelve shelve) {
        StringBuilder sb = new StringBuilder();
        sb.append("Shelve number: ").append(number);
        sb.append(" contains: ").append(shelve.getProduct().getName());
        sb.append(" count: ").append(shelve.availableProductsNumber()).append("\n");
        return shelve.availableProductsNumber() > 0 ? sb.toString() : "";
    }

}
