package tdd.vendingMachine.utils;

import tdd.vendingMachine.register.DenominationsOfCoins;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class Utils {

    public static <T> Boolean optGetBool(T t, Function<T, Boolean> provider) {
        return t != null ? provider.apply(t) : false;
    }

    public static <T, R> R optGetObject(T t, Predicate<T> predicate, Function<T, R> provider) {
        return predicate.test(t) ? provider.apply(t) : null;
    }

    public static <T> Integer optGetInt(T t, Predicate<T> predicate, Function<T, Integer> provider) {
        return predicate.test(t) ? provider.apply(t) : 0;
    }

    public static void addToList(DenominationsOfCoins denominationsOfCoins, int count, List<BigDecimal> destination) {
        IntStream.range(0, count).forEach($ -> destination.add(new BigDecimal(denominationsOfCoins.getValue())));
    }

    public static <T> void optDo(T t, Predicate<T> predicate, Consumer<T> consumer) {
        if (predicate.test(t)) {
            consumer.accept(t);
        }
    }

    public static BigDecimal sumRegister(Map<DenominationsOfCoins, Integer> register) {
        return register.keySet().stream()
            .map($ -> new BigDecimal($.getValue()).multiply(new BigDecimal(checkLevelOfCoins($, register))))
            .collect(Collectors.toList())
            .stream()
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static int checkLevelOfCoins(DenominationsOfCoins denominationsOfCoins, Map<DenominationsOfCoins, Integer> register) {
        return optGetInt(register, $ -> register.get(denominationsOfCoins) != null, $ -> register.get(denominationsOfCoins));
    }
}
