package tdd.vendingMachine.utils;

import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.products.*;
import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.shelves.IShelve;
import tdd.vendingMachine.shelves.Shelve;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 17.01.2017.
 */
public class InitMachine {
    public static String initMachine(VendingMachine machine) {
        initCashRegister();

        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("1.8"), "Coke"), 5));
        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("4.8"), "Nesty"), 5));
        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("1.2"), "Princessa"), 8));
        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("1.8"), "Coke"), 5));
        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("2.8"), "Pepsi"), 8));
        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("2.8"), "Pepsi"), 5));
        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("2.8"), "Snickers"), 8));
        machine.insertShelve(initShelves(machine, productCreator(new BigDecimal("1.8"), "Twix"), 2));

        return "OK";
    }

    private static void initCashRegister() {
        CashRegister register = CashRegister.getInstance();
        register.addMoney(DenominationsOfCoins.FIVE, 5);
        register.addMoney(DenominationsOfCoins.TWO, 5);
        register.addMoney(DenominationsOfCoins.ONE, 5);
        register.addMoney(DenominationsOfCoins.FIFTY_PENS, 5);
        register.addMoney(DenominationsOfCoins.TWENTY_PENS, 5);
        register.addMoney(DenominationsOfCoins.TEN_PENS, 5);

    }

    private static IProduct productCreator(BigDecimal price, String name) {
        IProductFactory cokeFactory = Coke::new;
        IProductFactory pepsiFactory = Pepsi::new;
        IProductFactory snickersFactory = Snickers::new;
        IProductFactory otherProductFactory = Product::new;

        switch (name) {
            case "Coke":
                return cokeFactory.createProduct(price, name);
            case "Pepsi":
                return pepsiFactory.createProduct(price, name);
            case "Snickers":
                return snickersFactory.createProduct(price, name);
            default:
                return otherProductFactory.createProduct(price, name);
        }
    }

    private static IShelve initShelves(VendingMachine machine, IProduct product, int availableNumber) {
        IShelve shelve = new Shelve();
        shelve.addProduct(product);
        shelve.addMoreProducts(availableNumber);
        return shelve;
    }
}
