package tdd.vendingMachine.shelves;

import tdd.vendingMachine.products.IProduct;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class Shelve implements IShelve{

    private int availableProductNumber = 0;
    private IProduct product;

    @Override
    public IProduct sellProduct() {
        if (availableProductNumber > 0) {
            availableProductNumber--;
            return product;
        }
        return null;
    }

    @Override
    public IProduct getProduct() {
        return product;
    }


    @Override
    public void addProduct(IProduct product) {
        this.product = product;
    }

    @Override
    public int soldProductsNumber() {
        return SHELVE_SIZE - availableProductNumber;
    }

    @Override
    public int availableProductsNumber() {
        return availableProductNumber;
    }

    @Override
    public void addMoreProducts(int number) {
        if (availableProductsNumber() + number > SHELVE_SIZE) {
            throw new IllegalArgumentException("To much products");
        }
        availableProductNumber = availableProductsNumber() + number;
    }
}
