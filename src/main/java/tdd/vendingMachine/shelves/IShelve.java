package tdd.vendingMachine.shelves;

import tdd.vendingMachine.products.IProduct;

/**
 * Created by Mateusz on 16.01.2017.
 */
public interface IShelve {
    int SHELVE_SIZE = 8;

    IProduct sellProduct();

    IProduct getProduct();

    void addProduct(IProduct product);

    int soldProductsNumber();

    int availableProductsNumber();

    void addMoreProducts(int number);
}
