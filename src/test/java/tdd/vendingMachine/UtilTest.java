package tdd.vendingMachine;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import tdd.vendingMachine.register.DenominationsOfCoins;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static tdd.vendingMachine.utils.Utils.addToList;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class UtilTest {

    @Test
    public void shouldAddBigDecimalsToArray() {
        List<BigDecimal> array = new ArrayList<>();
        array.add(new BigDecimal(DenominationsOfCoins.FIVE.getValue()));
        Assertions.assertThat(array.size()).describedAs("Array should contains one element").isEqualTo(1);

        addToList(DenominationsOfCoins.ONE, 5, array);
        Assertions.assertThat(array.size()).describedAs("Array should contains six elements").isEqualTo(6);
    }
}
