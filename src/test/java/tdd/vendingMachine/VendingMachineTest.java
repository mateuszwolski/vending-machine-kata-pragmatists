package tdd.vendingMachine;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.utils.InitMachine;

public class VendingMachineTest {

    CashRegister register = CashRegister.getInstance();


    @Test
    public void shouldValidateStateChanging() {
        VendingMachine vendingMachine = new VendingMachine();
        InitMachine.initMachine(vendingMachine);
        vendingMachine.selectProduct("5");
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should has state NoCoinsInsertedState").isEqualTo(vendingMachine.getNoCoinsInsertedState());
        vendingMachine.insertCoin(DenominationsOfCoins.TWENTY_PENS);
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should has state CoinsInsertedState").isEqualTo(vendingMachine.getCoinsInsertedState());
        vendingMachine.insertCoin(DenominationsOfCoins.TWENTY_PENS);
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should has state CoinsInsertedState").isEqualTo(vendingMachine.getCoinsInsertedState());
        vendingMachine.selectProduct("55");
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should has state CoinsInsertedState").isEqualTo(vendingMachine.getCoinsInsertedState());
        vendingMachine.selectProduct("5");
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should has state ProductChooseState").isEqualTo(vendingMachine.getProductChooseState());
        vendingMachine.insertCoin(DenominationsOfCoins.FIVE);
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should has state NoCoinsInsertedState").isEqualTo(vendingMachine.getNoCoinsInsertedState());
    }

    @Test
    public void noMoneyToChange() {
        VendingMachine vendingMachine = new VendingMachine();
        InitMachine.initMachine(vendingMachine);

        //clean cashRegistry and insert only one 5.0
        register.takeAllMoney();
        Assertions.assertThat(register.getRegister().keySet()).describedAs("CashRegister is empty").isEmpty();
        register.addMoney(DenominationsOfCoins.FIVE, 1);
        Assertions.assertThat(register.getRegister().keySet().size()).describedAs("CashRegister has one coin").isEqualTo(1);
        vendingMachine.insertCoin(DenominationsOfCoins.ONE);
        vendingMachine.insertCoin(DenominationsOfCoins.ONE);
        vendingMachine.insertCoin(DenominationsOfCoins.TWO);
        vendingMachine.selectProduct("5");
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should has state NoCoinsInsertedState").isEqualTo(vendingMachine.getNoCoinsInsertedState());
        machineInBasicState(vendingMachine);
        Assertions.assertThat(register.getRegister().keySet().size()).describedAs("CashRegister has one coin").isEqualTo(1);
        flush();
    }

    private void machineInBasicState(VendingMachine vendingMachine) {
        Assertions.assertThat(vendingMachine.getMachineState()).describedAs("Machine should change state to NoCoinsInsertedState").isEqualTo(vendingMachine.getNoCoinsInsertedState());
        Assertions.assertThat(vendingMachine.getChosenShelveNumber()).describedAs("Chosen product without choose is set up to -1").isEqualTo(-1);
        Assertions.assertThat(vendingMachine.getChosenShelve()).describedAs("Chosen product without choose is null").isNull();
        Assertions.assertThat(vendingMachine.getCurrentCoinsState().keySet()).describedAs("Current coins has to be empty").isEmpty();
    }

    private void flush() {
        CashRegister.getInstance().takeAllMoney();
    }
}
