package tdd.vendingMachine.states;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.utils.InitMachine;

/**
 * Created by Mateusz on 17.01.2017.
 */
public class CoinsInsertedStateTest {

    static VendingMachine MACHINE = new VendingMachine();

    @Before
    public void setUp() {
        flush();
        CashRegister.getInstance().takeAllMoney();
        InitMachine.initMachine(MACHINE);
        MACHINE.insertCoin(DenominationsOfCoins.ONE);
    }

    @Test
    public void shouldValidateInitState() {
        validateBaseMachineState(MACHINE);
    }

    @Test
    public void shouldValidateMachineStateAfterInsertCoin() {
        validateBaseMachineState(MACHINE);
        MACHINE.insertCoin(DenominationsOfCoins.ONE);
        validateBaseMachineState(MACHINE);

        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to CoinsInsertedState").isEqualTo(MACHINE.getCoinsInsertedState());
        Assertions.assertThat(MACHINE.getCurrentCoinsState().keySet().iterator().next()).describedAs("Current coins has to be empty").isEqualTo(DenominationsOfCoins.ONE);
        Assertions.assertThat(MACHINE.getCurrentCoinsState().get(DenominationsOfCoins.ONE)).describedAs("Current coins has to be empty").isEqualTo(2);

    }

    @Test
    public void shouldValidateMachineStateAfterChosenProductButton() {
        validateBaseMachineState(MACHINE);
        MACHINE.selectProduct("2");
        Assertions.assertThat(MACHINE.getChosenShelveNumber()).describedAs("Selected shelve number has to be 2").isEqualTo(2);
        Assertions.assertThat(MACHINE.getChosenShelve()).describedAs("Selected shelve has to be the same").isEqualTo(MACHINE.getShelves().get(2));
        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to ProductChooseState").isEqualTo(MACHINE.getProductChooseState());
    }

    @Test
    public void shouldValidateMachineStateAfterChosenProductButtonIncorrect() {
        validateBaseMachineState(MACHINE);
        MACHINE.selectProduct("222");
        Assertions.assertThat(MACHINE.getChosenShelveNumber()).describedAs("Selected shelve number has to be -1").isEqualTo(-1);
        Assertions.assertThat(MACHINE.getChosenShelve()).describedAs("Selected shelve has to be null").isNull();
        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should should stay in CoinsInsertedState").isEqualTo(MACHINE.getCoinsInsertedState());
    }

    @Test
    public void shouldValidateMachineStateAfterCancel() {
        validateBaseMachineState(MACHINE);
        MACHINE.cancelOperation();

        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to CoinsInsertedState").isEqualTo(MACHINE.getNoCoinsInsertedState());
        Assertions.assertThat(MACHINE.getCurrentCoinsState().keySet()).describedAs("Current coins has to be empty").isEmpty();
        Assertions.assertThat(MACHINE.getChosenShelveNumber()).describedAs("Chosen product without choose is set up to -1").isEqualTo(-1);
        Assertions.assertThat(MACHINE.getChosenShelve()).describedAs("Chosen product without choose is null").isNull();
        MACHINE.flushCurrentCoinsState();
    }

    @Test
    public void shouldValidateMachineStateAfterWaiting() {
        validateBaseMachineState(MACHINE);
        MACHINE.waiting();
        validateBaseMachineState(MACHINE);
        MACHINE.flushCurrentCoinsState();
    }

    private void validateBaseMachineState(VendingMachine machine) {
        Assertions.assertThat(machine.getChosenShelveNumber()).describedAs("Chosen product without choose is set up to -1").isEqualTo(-1);
        Assertions.assertThat(machine.getChosenShelve()).describedAs("Chosen product without choose is null").isNull();
        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to CoinsInsertedState").isEqualTo(MACHINE.getCoinsInsertedState());
        CashRegister.getInstance().getRegister().keySet().stream()
            .forEach($ -> Assertions.assertThat(CashRegister.getInstance().getRegister().get($)).describedAs("CashRegistry has to contains 5 time each denomination").isEqualTo(5));
        Assertions.assertThat(machine.getCurrentCoinsState().keySet()).describedAs("Current coins has to be empty").isNotEmpty();
        Assertions.assertThat(machine.getShelves().keySet().size()).describedAs("Machine has to contains 8 shelves").isEqualTo(8);
    }

    private void flush() {
        MACHINE = new VendingMachine();
        CashRegister.getInstance().takeAllMoney();
    }
}
