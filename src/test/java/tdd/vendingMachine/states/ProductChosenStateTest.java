package tdd.vendingMachine.states;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.utils.InitMachine;

/**
 * Created by Mateusz on 17.01.2017.
 */
public class ProductChosenStateTest {

    static VendingMachine MACHINE = new VendingMachine();

    @Before
    public void setUp() {
        flush();
        CashRegister.getInstance().takeAllMoney();
        InitMachine.initMachine(MACHINE);
        MACHINE.insertCoin(DenominationsOfCoins.ONE);
        MACHINE.selectProduct("2");
    }

    @Test
    public void shouldValidateInitState() {
        validateBaseMachineState(MACHINE);
    }

    @Test
    public void shouldValidateMachineStateAfterInsertCoin() {
        validateBaseMachineState(MACHINE);
        MACHINE.insertCoin(DenominationsOfCoins.FIVE);
        machineInBasicState();
        Assertions.assertThat(CashRegister.getInstance().getRegister().get(DenominationsOfCoins.FIVE)).describedAs("Register should contains 6 coins 5.0").isEqualTo(6);
        Assertions.assertThat(CashRegister.getInstance().getRegister().get(DenominationsOfCoins.ONE)).describedAs("Register should contains 4 coins 1.0").isEqualTo(5);
        Assertions.assertThat(CashRegister.getInstance().getRegister().get(DenominationsOfCoins.TWENTY_PENS)).describedAs("Register should contains 4 coins 0.2").isEqualTo(4);
    }

    @Test
    public void shouldValidateMachineStateAfterChosenProductButtonWithWrongValue(){
        validateBaseMachineState(MACHINE);
        MACHINE.selectProduct("33");
        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to CoinsInsertedState").isEqualTo(MACHINE.getCoinsInsertedState());
    }

    @Test
    public void shouldValidateMachineStateAfterChosenProductButton(){
        validateBaseMachineState(MACHINE);
        MACHINE.selectProduct("3");
        Assertions.assertThat(MACHINE.getCurrentCoinsState().get(DenominationsOfCoins.ONE)).describedAs("Denomination 1.0 is one time").isEqualTo(1);
        MACHINE.insertCoin(DenominationsOfCoins.ONE);
        machineInBasicState();
        Assertions.assertThat(CashRegister.getInstance().getRegister().get(DenominationsOfCoins.ONE)).describedAs("Register should contains 4 coins 1.0").isEqualTo(7);
        Assertions.assertThat(CashRegister.getInstance().getRegister().get(DenominationsOfCoins.TWENTY_PENS)).describedAs("Register should contains 4 coins 0.2").isEqualTo(4);
    }


    @Test
    public void shouldValidateMachineStateAfterCancel() {
        validateBaseMachineState(MACHINE);
        MACHINE.cancelOperation();
        machineInBasicState();
    }

    @Test
    public void shouldValidateMachineStateAfterWaiting() {
        validateBaseMachineState(MACHINE);
        MACHINE.waiting();
        validateBaseMachineState(MACHINE);
    }

    private void validateBaseMachineState(VendingMachine machine) {
        Assertions.assertThat(machine.getChosenShelveNumber()).describedAs("Chosen product without choose is set up to 2").isEqualTo(2);
        Assertions.assertThat(machine.getChosenShelve()).describedAs("Chosen product without choose is not null").isNotNull();
        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to ProductChosenState").isEqualTo(MACHINE.getProductChooseState());
        CashRegister.getInstance().getRegister().keySet().stream()
            .forEach($ -> Assertions.assertThat(CashRegister.getInstance().getRegister().get($)).describedAs("CashRegistry has to contains 5 time each denomination").isEqualTo(5));
        Assertions.assertThat(machine.getCurrentCoinsState().keySet()).describedAs("Current coins has not to be empty").isNotEmpty();
        Assertions.assertThat(machine.getShelves().keySet().size()).describedAs("Machine has to contains 8 shelves").isEqualTo(8);
    }

    private void machineInBasicState() {
        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to NoCoinsInsertedState").isEqualTo(MACHINE.getNoCoinsInsertedState());
        Assertions.assertThat(MACHINE.getChosenShelveNumber()).describedAs("Chosen product without choose is set up to -1").isEqualTo(-1);
        Assertions.assertThat(MACHINE.getChosenShelve()).describedAs("Chosen product without choose is null").isNull();
        Assertions.assertThat(MACHINE.getCurrentCoinsState().keySet()).describedAs("Current coins has to be empty").isEmpty();
    }


    private void flush() {
        MACHINE = new VendingMachine();
        CashRegister.getInstance().takeAllMoney();
    }
}
