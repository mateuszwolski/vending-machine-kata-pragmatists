package tdd.vendingMachine.states;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.register.CashRegister;
import tdd.vendingMachine.register.DenominationsOfCoins;
import tdd.vendingMachine.utils.InitMachine;

/**
 * Created by Mateusz on 17.01.2017.
 */
public class NoCoinsInsertedStateTest {

    static VendingMachine MACHINE = new VendingMachine();

    @Before
    public void setUp() {
        flush();
        CashRegister.getInstance().takeAllMoney();
        InitMachine.initMachine(MACHINE);
    }

    @Test
    public void shouldValidateInitState() {
        validateBaseMachineState(MACHINE);
    }

    @Test
    public void shouldValidateMachineStateAfterInsertCoin() {
        validateBaseMachineState(MACHINE);

        MACHINE.insertCoin(DenominationsOfCoins.ONE);

        Assertions.assertThat(MACHINE.getMachineState()).describedAs("Machine should change state to CoinsInsertedState").isEqualTo(MACHINE.getCoinsInsertedState());
        Assertions.assertThat(MACHINE.getCurrentCoinsState().keySet().iterator().next()).describedAs("Current coins has to be empty").isEqualTo(DenominationsOfCoins.ONE);
    }

    @Test
    public void shouldValidateMachineStateAfterChosenProductButton1() {
        validateBaseMachineState(MACHINE);
        MACHINE.selectProduct("1");
        validateBaseMachineState(MACHINE);
    }

    @Test
    public void shouldValidateMachineStateAfterChosenProductButton22() {
        validateBaseMachineState(MACHINE);
        MACHINE.selectProduct("22");
        validateBaseMachineState(MACHINE);
    }

    @Test
    public void shouldValidateMachineStateAfterChosenProductButtonTest() {
        validateBaseMachineState(MACHINE);
        MACHINE.selectProduct("Test");
        validateBaseMachineState(MACHINE);
    }

    @Test
    public void shouldValidateMachineStateAfterCancel() {
        validateBaseMachineState(MACHINE);
        MACHINE.cancelOperation();
        validateBaseMachineState(MACHINE);
    }

    @Test
    public void shouldValidateMachineStateAfterWaiting() {
        validateBaseMachineState(MACHINE);
        MACHINE.waiting();
        validateBaseMachineState(MACHINE);

    }

    private void validateBaseMachineState(VendingMachine machine) {
        Assertions.assertThat(machine.getChosenShelveNumber()).describedAs("Chosen product without choose is set up to -1").isEqualTo(-1);
        Assertions.assertThat(machine.getChosenShelve()).describedAs("Chosen product without choose is null").isNull();
        CashRegister.getInstance().getRegister().keySet().stream()
            .forEach($ -> Assertions.assertThat(CashRegister.getInstance().getRegister().get($)).describedAs("CashRegistry has to contains 5 time each denomination").isEqualTo(5));
        Assertions.assertThat(machine.getCurrentCoinsState().keySet()).describedAs("Current coins has to be empty").isEmpty();
        Assertions.assertThat(machine.getShelves().keySet().size()).describedAs("Machine has to contains 8 shelves").isEqualTo(8);
    }

    private void flush() {
        MACHINE = new VendingMachine();
        CashRegister.getInstance().takeAllMoney();
    }
}
