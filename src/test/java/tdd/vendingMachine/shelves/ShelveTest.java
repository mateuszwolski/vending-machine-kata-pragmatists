package tdd.vendingMachine.shelves;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import tdd.vendingMachine.products.IProduct;
import tdd.vendingMachine.shelves.IShelve;
import tdd.vendingMachine.products.Product;
import tdd.vendingMachine.shelves.Shelve;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class ShelveTest {

    @Test
    public void shouldReturnNullFromEmptyShelve() {
        IShelve shelve = new Shelve();
        Assertions.assertThat(shelve.sellProduct()).describedAs("Shelve should be null").isNull();
    }

    @Test
    public void shouldReturnOneProduct() {
        IShelve shelve = prepareShelve(2);

        Assertions.assertThat(shelve.sellProduct()).describedAs("Shelve should return product").isNotNull();
    }

    @Test
    public void shouldDecreaseNumberOfAvailableProducts() {
        IShelve shelve = prepareShelve(2);
        shelve.sellProduct();

        Assertions.assertThat(shelve.availableProductsNumber()).describedAs("Shelve should return information that one products are available").isEqualTo(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowException() {
        IShelve shelve = prepareShelve(9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionAgain() {
        IShelve shelve = prepareShelve(5);
        shelve.addMoreProducts(5);
    }


    private IShelve prepareShelve(int availableProducts) {
        IProduct product = new Product(new BigDecimal("2.2"), "Cola 330ml");
        IShelve shelve = new Shelve();
        shelve.addProduct(product);
        shelve.addMoreProducts(availableProducts);
        return shelve;
    }
}
