package tdd.vendingMachine.register;

import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class DenominationsOfCoinsTest {

    @Test
    public void shouldConfirmInputValue() {
        Assertions.assertThat(DenominationsOfCoins.isValueExists("0.5")).describedAs("DenominationsOfCoins contains 0.5 should return true").isTrue();
    }

    @Test
    public void shouldReturnFalseNoThatValue() {
        Assertions.assertThat(DenominationsOfCoins.isValueExists("0.6")).describedAs("DenominationsOfCoins not contain 0.6 should return false").isFalse();
    }
}
