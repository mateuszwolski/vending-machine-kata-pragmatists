package tdd.vendingMachine.register;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import tdd.vendingMachine.AdministratorPanel;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static tdd.vendingMachine.utils.Utils.checkLevelOfCoins;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class CashRegisterTest {
    CashRegister register = CashRegister.getInstance();

    @Test
    public void shouldAddCoinsToRegistry() {
        register.addMoney(DenominationsOfCoins.FIVE, 10);
        Assertions.assertThat(checkLevelOfCoins(DenominationsOfCoins.FIVE, register.getRegister())).describedAs("Registry should contains 10 coins of 5.0").isEqualTo(10);
        cleanRegistry(new BigDecimal("50"));
    }

    @Test
    public void shouldReturnZeroCoins() {
        Assertions.assertThat(checkLevelOfCoins(DenominationsOfCoins.ONE, register.getRegister())).describedAs("Registry should contains 0 coins of 1.0").isEqualTo(0);
        cleanRegistry(new BigDecimal("0"));
    }

    @Test
    public void shouldAddAmountOfCoins() {
        register.addMoney(DenominationsOfCoins.TEN_PENS, 10);
        Assertions.assertThat(checkLevelOfCoins(DenominationsOfCoins.TEN_PENS, register.getRegister())).describedAs("Registry should contains 10 coins of 0.1").isEqualTo(10);

        register.addMoney(DenominationsOfCoins.TEN_PENS, 5);
        Assertions.assertThat(checkLevelOfCoins(DenominationsOfCoins.TEN_PENS, register.getRegister())).describedAs("Registry should contains 15 coins of 5.0").isEqualTo(15);
        cleanRegistry(new BigDecimal("1.5"));
    }

    @Test
    public void shouldReturnChange2_7() {
        register.addMoney(DenominationsOfCoins.TWO, 2);
        register.addMoney(DenominationsOfCoins.TWENTY_PENS, 2);
        register.addMoney(DenominationsOfCoins.FIFTY_PENS, 2);

        Assertions.assertThat(register.isItPossibleToReturnChange(new BigDecimal("7.7"), new BigDecimal("5.0"))).describedAs("Change return should be possible 2.7").isTrue();
        cleanRegistry(new BigDecimal("5.4"));
    }

    @Test
    public void shouldReturnChange0_4() {
        register.addMoney(DenominationsOfCoins.TWO, 2);
        register.addMoney(DenominationsOfCoins.TWENTY_PENS, 1);
        register.addMoney(DenominationsOfCoins.FIFTY_PENS, 2);
        register.addMoney(DenominationsOfCoins.TEN_PENS, 2);


        Assertions.assertThat(register.isItPossibleToReturnChange(new BigDecimal("5.4"), new BigDecimal("5.0"))).describedAs("Change return should be possible 0.4").isTrue();
        cleanRegistry(new BigDecimal("5.4"));
    }

    @Test
    public void shouldNotReturnChange0_4() {
        register.addMoney(DenominationsOfCoins.TWO, 2);
        register.addMoney(DenominationsOfCoins.TWENTY_PENS, 1);
        register.addMoney(DenominationsOfCoins.FIFTY_PENS, 2);
        register.addMoney(DenominationsOfCoins.TEN_PENS, 1);


        Assertions.assertThat(register.isItPossibleToReturnChange(new BigDecimal("5.4"), new BigDecimal("5.0"))).describedAs("Change return should not be possible 0.4").isFalse();
        cleanRegistry(new BigDecimal("5.3"));
    }

    @Test
    public void shouldReturnChange0_9() {
        register.addMoney(DenominationsOfCoins.FIVE, 2);
        register.addMoney(DenominationsOfCoins.TWO, 2);
        register.addMoney(DenominationsOfCoins.TWENTY_PENS, 1);
        register.addMoney(DenominationsOfCoins.FIFTY_PENS, 2);
        register.addMoney(DenominationsOfCoins.TEN_PENS, 2);


        Assertions.assertThat(register.isItPossibleToReturnChange(new BigDecimal("5.9"), new BigDecimal("5.0"))).describedAs("Change return should be possible 0.9").isTrue();
        cleanRegistry(new BigDecimal("15.4"));
    }

    @Test
    public void shouldSumAllMoney() {
        register.addMoney(DenominationsOfCoins.FIVE, 2);
        register.addMoney(DenominationsOfCoins.TWO, 2);
        cleanRegistry(new BigDecimal("14"));
    }

    @Test
    public void shouldReturnTwiceTenPens() {
        register.addMoney(DenominationsOfCoins.FIVE, 2);
        register.addMoney(DenominationsOfCoins.TEN_PENS, 2);
        Map<DenominationsOfCoins, Integer> change = register.changeMoney(new BigDecimal("1.2"), new BigDecimal("1"));
        Map<DenominationsOfCoins, Integer> expected = new HashMap<>();
        expected.put(DenominationsOfCoins.TEN_PENS, 2);
        Assertions.assertThat(change).describedAs("Change should contains twice 0.1").isEqualTo(expected);
        cleanRegistry(new BigDecimal("10.2"));
    }

    @Test
    public void shouldNotifyAdminPanel() {
        AdministratorPanel ap = new AdministratorPanel();
        register.addObserver(ap);
        register.addMoney(DenominationsOfCoins.FIVE, 3);
        register.removeMoney(DenominationsOfCoins.FIVE, 3);
    }

    private void cleanRegistry(BigDecimal expected) {
        Assertions.assertThat(register.takeAllMoney()).describedAs("Register should return " + expected.toPlainString() + " coins").isEqualByComparingTo(expected);

    }
}
