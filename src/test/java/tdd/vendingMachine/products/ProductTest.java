package tdd.vendingMachine.products;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by Mateusz on 16.01.2017.
 */
public class ProductTest {

    @Test
    public void shouldSetUpProduct() {
        IProduct product = new Product(new BigDecimal("2.2"), "Cola 330ml");
        Assertions.assertThat(product.getName()).describedAs("Product has to have a name").isNotEmpty();
        Assertions.assertThat(product.getPrice()).describedAs("Product has to have a price").isGreaterThan(new BigDecimal(0));
    }

    @Test
    public void shouldSetUpNewPrice() {
        IProduct product = new Product(new BigDecimal("2.2"), "Cola 330ml");
        Assertions.assertThat(product.getPrice()).describedAs("Product has to have a price").isEqualByComparingTo(new BigDecimal("2.2"));

        product.changePrice(new BigDecimal("2.4"));
        Assertions.assertThat(product.getPrice()).describedAs("Product has to have new price").isEqualByComparingTo(new BigDecimal("2.4"));
    }
}
